import com.jentity.JQL;
import com.jentity.command.Select;
import com.jentity.command.SelectionType;
import com.jentity.starter.Jentity;
import com.jentity.structures.ForeignKey;
import com.jentity.structures.Table;

import java.io.IOException;

import static com.jentity.command.From.from;

/**
 * Created by yusuf on 1/8/2015.
 */
public class Test {

    @org.junit.Test
    public void test() throws IOException, ClassNotFoundException {

        Jentity jentity = new Jentity("deneme");
        Table table = jentity.getTable("CARS");
        JQL jql = JQL.createJQL()
                    .select(Select.selectBy(SelectionType.ALL))
                    .from(from(table));
        jql.execute().getQuery();

    }
}
