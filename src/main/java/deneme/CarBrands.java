package deneme;

import com.jentity.annotation.Column;
import com.jentity.annotation.Table;

/**
 * Created by yusuf on 1/20/2015.
 */
@Table(tableName = "CARBRANDS")
public class CarBrands {

    @Column(columnName = "carBrandId",type = Column.Type.INT ,unique = true)
    private int carBrandId;

    @Column(columnName = "carBrandId",type = Column.Type.STRING )
    private String carBrand;
}
