package deneme;

import com.jentity.annotation.Column;
import com.jentity.annotation.Table;

/**
 * Created by yusuf on 1/20/2015.
 */
@Table(tableName = "CARTYPE")
public class CarType {
    @Column(unique = true,columnName = "carTypeId",type = Column.Type.INT)
    private int carTypeId;

    @Column(columnName = "carType",type = Column.Type.STRING)
    private String carType;
}
