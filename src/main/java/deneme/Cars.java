package deneme;

import com.jentity.annotation.Column;
import com.jentity.annotation.ForeignKey;
import com.jentity.annotation.Table;

/**
 * Created by yusuf on 1/20/2015.
 */

@Table(tableName = "CARS",forignKeys = {
        @ForeignKey(tableName = "CARTYPE",columnName = "typeOfCar"),
        @ForeignKey(tableName = "CARBRANDS",columnName = "brandOfCar")
})
public class Cars {

    @Column(type = Column.Type.INT, unique = true, columnName = "carId")
    private int carId;

    @Column(type = Column.Type.INT, columnName = "carName")
    private String carName;

    @Column(type = Column.Type.INT, columnName = "typeOfCar")
    private int typeOfCar;

    @Column(type = Column.Type.INT, columnName = "brandOfCar")
    private int brandOfCar;

    @Column(type = Column.Type.INT, columnName = "ownerOfCar")
    private int ownerOfCar;
}
