package deneme;

import com.jentity.annotation.Column;
import com.jentity.annotation.ForeignKey;
import com.jentity.annotation.Table;

/**
 * Created by yusuf on 1/19/2015.
 */

@Table(tableName = "adress")
public class Adress {

    @Column(columnName = "addressId", type = Column.Type.INT, unique = true)
    private String addressId;

    @Column(columnName = "addressId", type = Column.Type.STRING)
    private String addressDescription;
}
