package com.jentity.builder;

import com.jentity.annotation.Column;

import java.lang.reflect.Field;

/**
 * Created by yusuf on 1/11/2015.
 */
public class QueryBuilder {
    private String SELECT = "SELECT";

    public static QueryBuilder build(){
        return new QueryBuilder();
    }

    public <T> QueryBuilder select(T... t){

        for (T tC : t){
            for(Field field : tC.getClass().getDeclaredFields())
            {

                field.setAccessible(true);

                Column column = field.getAnnotation(Column.class);
                System.out.print(column.columnName() + " " );

                try {
                    System.out.print(field.get(tC));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                System.out.println();
            }
            System.err.println("------------------------");
        }
        return this;
    }

}
