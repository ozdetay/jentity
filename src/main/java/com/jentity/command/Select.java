package com.jentity.command;

/**
 * Created by yusufaltun on 30/01/15.
 */
public class Select {

    private SelectionType selectionType;

    public static Select selectBy(SelectionType selectionType){
        return new Select(selectionType);
    }

    public Select(SelectionType selectionType){
        this.selectionType = selectionType;
    }

    public String getString(){
        return null;
    }

    public SelectionType getSelectionType(){
        return selectionType;
    }
}
