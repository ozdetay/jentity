package com.jentity.command;

import com.jentity.structures.Table;

/**
 * Created by yusufaltun on 30/01/15.
 */
public class From {

    private Table[] tables;
    private Join join;

    public static From from(Table ... tables){
        return new From(tables);
    }

    public From(Table ... tables){
        this.tables = tables;
    }

    public From join(Join join){
        this.join = join;

        return this;
    }

    public Table[] getTables(){
        return tables;
    }
}
