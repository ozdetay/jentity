package com.jentity.command;

/**
 * Created by yusufaltun on 30/01/15.
 */
public enum Join {
    INNER("inner"),
    LEFT("left"),
    RIGHT("right"),
    OUTHER("outher");

    private String join;

    private Join(String join) {
        this.join = join;
    }

    public String getJoin() {
        return join;
    }

}
