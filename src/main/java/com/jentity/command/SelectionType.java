package com.jentity.command;

/**
 * Created by yusufaltun on 30/01/15.
 */
public enum SelectionType {

    ALL("*"),
    FIRST("first"),
    LAST("last");

    private String selectioType;

    private SelectionType(String s) {
        selectioType = s;
    }

    public String SelectionType() {
        return selectioType;
    }
}
