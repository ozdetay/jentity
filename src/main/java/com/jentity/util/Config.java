package com.jentity.util;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created with IntelliJ IDEA.
 * User: yusufaltun
 * Date: 26/01/15
 * Time: 10:30
 * To change this template use File | Settings | File Templates.
 */
public class Config {

    public static String getCurrentPath(){
        return System.getProperty("user.dir");
    }

    public static void loadLog4jProperties(){
        PropertyConfigurator.configure(getCurrentPath() + "/src/log4j.properties");
    }
}
