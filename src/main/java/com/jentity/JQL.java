package com.jentity;

import com.jentity.command.Executer;
import com.jentity.command.From;
import com.jentity.command.Select;

/**
 * Created with IntelliJ IDEA.
 * User: yusufaltun
 * Date: 26/01/15
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */
public class JQL {

    private Select select;
    private From from;

    public static JQL createJQL(){
        return new JQL();
    }

    public JQL select(Select select){
        this.select = select;
        return this;
    }

    public JQL from(From from){
        this.from = from;
        return this;
    }

    public JQL where(){
        return this;
    }

    public JQL join(){
        return this;
    }

    public Executer execute(){
        Executer executer = new Executer(this);
        return executer;
    }
    public Select getSelect(){
        return select;
    }
    public From getFrom(){
        return from;
    }
}
