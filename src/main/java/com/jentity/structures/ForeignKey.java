package com.jentity.structures;

/**
 * Created by yusuf on 1/24/2015.
 */
public class ForeignKey {

    private String columnName;
    private String table;

    public ForeignKey (String columnName, String table){
        this.columnName = columnName;
        this.table = table;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
