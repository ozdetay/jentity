package com.jentity.structures;

import java.util.List;

/**
 * Created by yusuf on 1/24/2015.
 */
public class Table {
    private String tableName;
    private String className;
    private List<ForeignKey> foreignKeys;

    public Table(String tableName, String className, List<ForeignKey> foreignKeys){
        this.tableName = tableName;
        this.className = className;
        this.foreignKeys = foreignKeys;
    }


    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ForeignKey> getForeignKeys() {
        return foreignKeys;
    }

    public void setForeignKeys(List<ForeignKey> foreignKeys) {
        this.foreignKeys = foreignKeys;
    }
}
