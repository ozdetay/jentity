package com.jentity.annotation;

/**
 * Created by yusuf on 1/19/2015.
 */
public @interface ForeignKey {

    public String tableName();
    public String columnName();
}
