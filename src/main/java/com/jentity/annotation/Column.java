package com.jentity.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by yusuf on 1/8/2015.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {

    public enum Type{
        INT, STRING, DATE
    }

    public boolean unique() default false;
    public Type type();
    public String columnName();

}
