package com.jentity.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by yusuf on 1/11/2015.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {

    public String tableName() ;
    public ForeignKey[] forignKeys() default @ForeignKey(tableName = "", columnName = "");
}
