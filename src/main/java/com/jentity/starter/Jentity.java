package com.jentity.starter;

import com.jentity.Finder;
import com.jentity.JQL;
import com.jentity.structures.ForeignKey;
import com.jentity.structures.Table;
import com.jentity.util.Config;
import org.apache.log4j.Logger;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusuf on 1/24/2015.
 */
public class Jentity {

    final static Logger logger = Logger.getLogger(Jentity.class);

    private List<Table> tables = new ArrayList<Table>();

    public Jentity(String entityPackage){
        loadTables(entityPackage);
    }
    public Jentity(){
        loadTables("");
    }

    private void loadTables(String entityPackage){

        Config.loadLog4jProperties();

        Finder finder = new Finder();
        for (Class cls : finder.getClasses(entityPackage))  {

            if(cls.isAnnotationPresent(com.jentity.annotation.Table.class)) {

                Annotation annotationForTable = cls.getAnnotation(com.jentity.annotation.Table.class);
                com.jentity.annotation.Table table = (com.jentity.annotation.Table) annotationForTable;

                com.jentity.annotation.ForeignKey foreignKey[] = table.forignKeys();

                List<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();
                for (com.jentity.annotation.ForeignKey k : foreignKey){
                    if (!k.columnName().equals("") && !k.tableName().equals("")) {
                        foreignKeys.add(new ForeignKey(k.columnName(), k.tableName()));
                    }
                }

                logger.warn(table.tableName() + " table is loaded");
                tables.add(new Table(table.tableName(), cls.getName(), foreignKeys));

            }
        }
    }

    public Table getTable(String tableName){
        for (Table table : tables){

            if (table.getTableName().equals(tableName)){
                return table;
            }
        }

        logger.error("Table not found");
        return null;
    }

    public JQL createJQL(){
        return new JQL();
    }
}
